package com.ktyp.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        System.out.println("--------------------");
        Plane boeing = new Plane("Boeing", "Rosario");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        boeing.left();
        boeing.right();
        boeing.up();
        boeing.down();

        System.out.println("--------------------");
        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();

        System.out.println("--------------------");
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();

        System.out.println("--------------------");
        Bat mavis = new Bat("Mavis");
        mavis.eat();
        mavis.sleep();
        mavis.takeoff();
        mavis.fly();
        mavis.landing();

        System.out.println("--------------------");
        Rat jerry = new Rat("Jerry");
        jerry.eat();
        jerry.sleep();
        jerry.walk();
        jerry.run();
        jerry.swim();

        System.out.println("--------------------");
        Cat tom = new Cat("Tom");
        tom.eat();
        tom.sleep();
        tom.walk();
        tom.run();
        tom.swim();

        System.out.println("--------------------");
        Dog snoopy = new Dog("Snoopy");
        snoopy.eat();
        snoopy.sleep();
        snoopy.walk();
        snoopy.run();
        snoopy.swim();

        System.out.println("--------------------");
        Crocodile crocadoo  = new Crocodile("Crocadoo");
        crocadoo.eat();
        crocadoo.sleep();
        crocadoo.swim();
        crocadoo.craw();

        System.out.println("--------------------");
        Fish nemo  = new Fish("Nemo");
        nemo.eat();
        nemo.sleep();
        nemo.swim();

        System.out.println("--------------------");
        Snake nicky = new Snake("Nicky");
        nicky.eat();
        nicky.sleep();
        nicky.craw();
        nicky.swim();

        System.out.println("--------------------");
        Bus bus = new Bus("Bus","Bus");
        bus.left();
        bus.right();
        bus.up();
        bus.down();

        System.out.println("--------------------");
        Submarine dum = new Submarine("Dum","Num");
        dum.left();
        dum.right();
        dum.up();
        dum.down();
        dum.swim();

        System.out.println("-----We can fly-----");
        Flyable[] flyables = {bird1, boeing, clark, mavis};
        for(int i=0; i<flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println("-----We can walk-----");
        Walkable[] walkables = {bird1, clark, man1, jerry, tom, snoopy};
        for(int i=0; i<walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        System.out.println("-----We can craw-----");
        Crawable[] crawables = {crocadoo, nicky};
        for(int i=0; i<crawables.length; i++) {
            crawables[i].craw();
        }

        System.out.println("-----We can swim-----");
        Swimable[] swimables = {man1, clark, tom, snoopy, crocadoo, nemo, nicky, dum};
        for(int i=0; i<swimables.length; i++) {
            swimables[i].swim();
        }

        System.out.println("-----We can move-----");
        Moveable[] moveable = {boeing, bus, dum};
        for(int i=0; i< moveable.length; i++) {
            moveable[i].left();
            moveable[i].right();
            moveable[i].up();
            moveable[i].down();
        }
    }
}
