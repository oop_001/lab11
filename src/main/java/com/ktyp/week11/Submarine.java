package com.ktyp.week11;

public class Submarine extends Vehicle implements Swimable, Moveable{

    public Submarine(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Submarine("+this.getName()+")";
    }

    @Override
    public void left() {
        System.out.println(this.toString() + " left.");
    }

    @Override
    public void right() {
        System.out.println(this.toString() + " right.");  
    }

    @Override
    public void up() {
        System.out.println(this.toString() + " up.");  
    }

    @Override
    public void down() {
        System.out.println(this.toString() + " down");
    }

    @Override
    public void swim() {
        System.out.println(this+ " swim."); 
    }
    
}
