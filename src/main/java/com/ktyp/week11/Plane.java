package com.ktyp.week11;

public class Plane extends Vehicle implements Flyable, Moveable{

    public Plane(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Plan("+getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");
    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
    }

    @Override
    public void left() {
        System.out.println(this.toString() + " left.");
    }

    @Override
    public void right() {
        System.out.println(this.toString() + " right.");  
    }

    @Override
    public void up() {
        System.out.println(this.toString() + " up.");  
    }

    @Override
    public void down() {
        System.out.println(this.toString() + " down");
    }
    
}
